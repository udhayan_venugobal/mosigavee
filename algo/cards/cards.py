# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:25:38 2015

@author: aveekmukherjee
"""

inp = [10,5,6,7]
player1 = "P1"
player2 = "P2"

P1cards = [0,0,0,0]
P2cards = [0,0,0,0]

def winning_sequence_sum(inp , player , index):
    print("index ="+str(index) + " p1=" + str(P1cards) + " p2=" + str(P2cards) )
    if (player == player1) :
        otherplayer = player2
        deck = P1cards
    else :
        otherplayer = player1
        deck = P2cards
    
    if(len(inp) == 2):
        #print (player + " choses card ="+ str(max(inp)))
        #print (otherplayer + " choses card ="+ str(min(inp)))
        P1cards[index] = max(inp)
        P2cards[index] = min(inp)
        return max(inp)
    
    #if we have a bigger set of input we have the below cases
    #case 1: player choses left card , other player gets to play from remaining deck
    win1 = winning_sequence_sum(inp[1:] , otherplayer , index+1)
    los1 =  sum(inp[1:]) - win1;
    case1 = inp[0] + los1
    
    #case 2: player choses right card, other player gets to play from remaining deck
    winn = winning_sequence_sum(inp[:-1] , otherplayer , index+1)
    losn = sum(inp[:-1]) - winn
    case2 = inp[len(inp)-1] + losn
    
    #which case is better for player 1?
    if case1 > case2:
        #print(player+ " choses card =" + str(inp[0]))
        #let's update the deck
        deck[index] = inp[0]
        return case1
    else:
        #print(player+ " choses card =" + str(inp[len(inp)-1]))
        deck[index] = inp[len(inp)-1]
        return case2
    
    
    

print(player1 + " gets to decide who goes first")
print("Winn = " + str(winning_sequence_sum(inp , player1 , 0)))
print("Player1: " +str(P1cards) + "Player2: " +str(P2cards))