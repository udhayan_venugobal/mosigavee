# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:25:38 2015

@author: aveekmukherjee
"""

inp_g = [10,5,6,7]
player1 = "P1"
player2 = "P2"

result = [[0 for x in range(len(inp_g))] for x in range(len(inp_g))]

def winning_sequence_sum(start , end , player):
    if(result[start][end] != 0):
        return result[start][end]
        
    inp = inp_g[start:end+1]
    ln = len(inp)
    if (player == player1) :
        otherplayer = player2
    else :
        otherplayer = player1
    
    if(len(inp) == 2):
        #print (player + " choses card ="+ str(max(inp)))
        #print (otherplayer + " choses card ="+ str(min(inp)))
        if(inp[0] > inp[1]):
            result[start][end]  = (inp[0],0)
        else:
            result[start][end]  = (inp[1],1)
        return result[start][end] 
    
    #if we have a bigger set of input we have the below cases
    #case 1: player choses left card , other player gets to play from remaining deck
    win1,card = winning_sequence_sum(start+1 , end , otherplayer)
    los1 =  sum(inp[1:]) - win1;
    case1 = inp[0] + los1
    
    #case 2: player choses right card, other player gets to play from remaining deck
    winn,card = winning_sequence_sum(start , end-1 , otherplayer)
    losn = sum(inp[:-1]) - winn
    case2 = inp[len(inp)-1] + losn
    
    #if ((los1+losn) > (win1+winn)):
        #print(player + "should not chose first")
    #which case is better for player 1?
    if case1 > case2:
        #print(player+ " choses card =" + str(inp[0]))
        result[start][end] = (case1,start)
    else:
        #print(player+ " choses card =" + str(inp[len(inp)-1]))
        result[start][end] = (case2,end)
    return result[start][end]
    
   
#def find_card_sequence(start , end):
 #   result[]
    
    

print(player1 + " gets to decide who goes first")
print("Winn = " + str(winning_sequence_sum(0, len(inp_g)-1 , player1)))
print(result)