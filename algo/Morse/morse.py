# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:46:54 2015

@author: aveekmukherjee
"""

#Algorithm morse code


#Algo
#1: Read the dictioanary file
#2: Try to match each word to the Morse string : can be optimized
#3: For each match add to the word_matrix and store its starting position and ending position
#4: Create a graph from this word_matrix, to find out which word follows whom
#5: Do a DFS on this graph and find the number of paths connecting the start to the end

Input_file = "APP4-Morse/morse-long.input"
morse_str = ""

def convert_word_to_morse(word):
    morse = ""
    for w in word:
        if w == 'A':
            morse += ".-"
        elif w == 'B':
            morse += "-..."
        elif w == 'C':
            morse += "-.-."
        elif w == 'D':
            morse += "-.."
        elif w == 'E':
            morse += "."
        elif w == 'F':
            morse += "..-."
        elif w == 'G':
            morse += "--."
        elif w == 'H':
            morse += "...."
        elif w == 'I':
            morse += ".."
        elif w == 'J':
            morse += ".---"
        elif w == 'K':
            morse += "-.-"
        elif w == 'L':
            morse += ".-.."
        elif w == 'M':
            morse += "--"
        elif w == 'N':
            morse += "-."
        elif w == 'O':
            morse += "---"
        elif w == 'P':
            morse += ".--."
        elif w == 'Q':
            morse += "--.-"
        elif w == 'R':
            morse += ".-."
        elif w == 'S':
            morse += "..."
        elif w == 'T':
            morse += "-"
        elif w == 'U':
            morse += "..-"
        elif w == 'V':
            morse += "...-"
        elif w == 'W':
            morse += ".--"
        elif w == 'X':
            morse += "-..-"
        elif w == 'Y':
            morse += "-.--"
        elif w == 'Z':
            morse += "--.."
    return morse

#we can store start , end
word_matrix=[]

"""
complexity : haystack L , needle M : 20
complexity of find: MlogL
Wrost case complexity of finding all substrings : M*MLogL
"""
def findallsubstr(word_matrix , haystack , needle):
    needle_morse = convert_word_to_morse(needle)
    index = 0
    l = len(needle_morse)
    while index < len(haystack):
        index = haystack.find(needle_morse, index)
        if index == -1:
            break
        word_matrix.append([needle ,index , index+l] )
        index += 1

"""
For all words we call findsubstr
Complexity = N*M2logL
"""
with open(Input_file, "r") as infile:
    morse_str = infile.readline()
    num_lines = infile.readline()
    for line in infile:
        findallsubstr(word_matrix ,morse_str , line)        
    
"""
Space comlexity : 
worst case N*L
"""
#print(word_matrix)

"""
Let's compute the size of the graph
each word forms a distinct edge
Edges: N*L
Vertices: Maximum number of vertices are possible 
if all indexes form vertices
= L
"""
graph = {}
#let's build a "multigraph" with this word matrix
def build_graph():
    for w in word_matrix:
        key = str(w[1])
        if key in graph:
            graph[key].append(str(w[2]))
        else:
            graph[key] = [str(w[2])]


"""
If I have V vertices in my graph:
Worst case : It's a complete graph. All vertices are connected to all other vertices
Therefore complexity is mul (1toV) k*(k-1)
CpDFS(k)= k*CpDFS(k-1)
= n*(n-1)*(n-2)...1
= O(V!)

but v = L , therefore v! = L!
"""
l = len(morse_str)

memo_path = {}

def count_paths_DFS(graph, start , end): 
    global memo_path
    #print("count_paths_DFS",start,end)
    
    if(start+"+"+end in memo_path):
        return memo_path[start+"+"+end]
    
    count_paths = 0
    
    #terminating condition:
    if(start == end):
        memo_path[start+"+"+end] =1
        return 1;
    #second termination condition
    if not start in graph:
        #print(start , "not in graph")
        return 0;
        
    for n in graph[start]:
        c = count_paths_DFS(graph , n , end)
        memo_path[n+"+"+end] = c
        count_paths += c
        
    memo_path[start+"+"+end] = count_paths
    return count_paths
           

build_graph()
#print(graph)
#print(memo_path)
print(count_paths_DFS(graph , '0' , str(l-1)))

#print(memo_path)
    