################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../code/main.c \
../code/maze.c \
../code/svg.c 

O_SRCS += \
../code/main.o \
../code/maze.o \
../code/svg.o 

OBJS += \
./code/main.o \
./code/maze.o \
./code/svg.o 

C_DEPS += \
./code/main.d \
./code/maze.d \
./code/svg.d 


# Each subdirectory must supply rules for building sources it contributes
code/%.o: ../code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


