#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void* land(void * dummy);

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t runway = PTHREAD_COND_INITIALIZER;
int runway_busy=0;

void main(int argc , char ** argv)
{
	pthread_mutex_init(&mutex, NULL);
	int max_flights = 1;
	int i =0;

	if(argc > 1){
		max_flights = atoi(argv[1]);
	}

	pthread_t *tids = malloc (max_flights*sizeof(pthread_t));

	for (i = 0 ; i < max_flights; i++){
		pthread_create (&tids[i], NULL, land, NULL ) ;
	}

	/* Wait until every thread ened */
	  for (i = 0; i < max_flights; i++){
		pthread_join (tids[i], NULL) ;
	  }
}

void* land(void * dummy)
{
	//I should allow to land only if no one using the runway
	pthread_mutex_lock(&mutex);
	while(runway_busy == 1){
		printf("Runway busy!! releasing mutex\n");
		pthread_cond_wait(&runway , &mutex);
	}

	runway_busy = 1;
	pthread_mutex_unlock(&mutex);

	//I am here means I have access to the runway
	//Let's pretend that I am using the runway by sleeping
	printf("Landing!!\n");
	sleep(5);

	//Now that i have used the runway I need to notify that I am done using the runway
	pthread_mutex_lock(&mutex);
	runway_busy = 0;
	pthread_cond_signal(&runway);
	pthread_mutex_unlock(&mutex);

	return NULL;
}
