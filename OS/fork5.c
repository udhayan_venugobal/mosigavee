#include <stdio.h>
#include <stdlib.h>

void main(int argc , char** argv)
{
	int f=0,loop;
	
	int n = atoi(argv[1]);
	printf("looping %d times\n", n);
	
	for (loop = 1 ; loop <=n ; loop++)
	{
		int pipefd[2];
		pipe(pipefd);
		int ran;
				
		f=fork();
		if(f==0){
			srand(getpid());
			ran = rand();
			
			printf("I am child %d my pid: %d ,my generated number is %d \n" ,loop, getpid(), ran);
			
			
			//I want to send to my parent
			write(pipefd[1], &ran , sizeof(ran));
			
			exit(0);
		}else 
		{
			//I am parent 
			int j;
			close(pipefd[1]);
			read(pipefd[0],&j, sizeof(int));
				
			printf("I am parent %d my max is : %d\n" ,loop, j);
			waitpid(f);
		}
	}		
}
