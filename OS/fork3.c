#include <stdio.h>

void main()
{
	int pipefd[2];

	int f=0;

	pipe(pipefd);

	f=fork();

	if(f==0)
	{
		int j;
		close(pipefd[1]);
		read(pipefd[0],&j, sizeof(int));
		printf("I am child , my message is : %d\n" , j);
	}
	else{
		printf("I am parent\n");
		int i = getpid();
		close(pipefd[0]);
		write(pipefd[1] , &i , sizeof(i));
	}
}
