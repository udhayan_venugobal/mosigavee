#include <stdio.h>
#include <stdlib.h>

void main(int argc , char** argv)
{
	int pipefd[2];

	int f=0,loop;
	
	int n = atoi(argv[1]);
	printf("looping %d times\n", n);
	
	pipe(pipefd);
	
	for (loop = 1 ; loop <=n ; loop++)
	{
		f=fork();
		if(f==0){
			srand(getpid());
			int j, ran = rand();
			//close(pipefd[1]);
			//read(pipefd[0],&j, sizeof(int));
			printf("I am child %d my pid: %d ,my generated number is %d \n" ,loop, getpid(), ran);
			
			/*
			if(loop == n)
			{
				//I want to send to process 0
				write(pipefd[1], &ran , sizeof(ran));
			}else{
				//I want to send to my next process
				write(pipefd[1], &ran , sizeof(ran));
			}*/
			
			exit(0);
		}else 
		{
			waitpid(f);
		}
	}
	
	//I am parent 
	int j;
	//close(pipefd[1]);
	//read(pipefd[0],&j, sizeof(int));
	//printf("I am parent my message is : %d\n" , j);
		
}
