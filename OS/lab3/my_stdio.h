#ifndef MYSTDIO_H
#define  MYSTDIO_H
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_SIZE 1024

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _b : _a; })
#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

typedef struct my_file{
	FILE * fp;
	char * buffer;
	int buffer_read_head;
	int buffer_read_tail;
	int size_dirty_bytes;
	char mode[2];
} MY_FILE;

/*
 * This is our buffered output implementation
 */

/*
 * Opens an access to a file, where name is the path to the file and mode is either ”r” for read or ”w” for
 * write. Returns a pointer to a MY_FILE structure upon success and NULL otherwise.
 */
MY_FILE *my_fopen(char *name, char *mode);

/*
 * Closes the access to the file associated to f. Returns 0 upon success and -1 otherwise.
 */
int my_fclose(MY_FILE *f);

/*
 * Reads at most nbelem elements of size size from file access f, that has to have been opened with
 * mode ”r”, and places them at the address pointed by p. Returns the number of elements actually read,
 * 0 if an end-of-file has been encountered before any element has been read and -1 if an error occurred.
 */
int my_fread(void *p, size_t size, size_t nbelem, MY_FILE *f);

/*
 * Writes at most nbelem elements of size size to file access f, that has to have been opened with mode
 * Writes ”w”, taken at the address pointed by p. Returns the number of elements actually written and -1 if an
 * Writes error occured.
 */
int my_fwrite(void *p, size_t taille, size_t nbelem, MY_FILE *f);

/*
 * Returns 1 if an end-of-file has been encountered during a previous read and 0 otherwise.
 */
int my_feof(MY_FILE *f);


/*
 int my_fwrite(void *p, size_t taille, size_t nbelem, MY_FILE *f){
	int total_size_requested =  taille * nbelem;
	int data_in_buffer = f->buffer_read_tail - f->buffer_read_head;

	//Let's find out how much total data can we fit?
	//Or rather how much we cannot fit in the buffer anymore?
	int data_size_cannot_fit = BUFFER_SIZE - data_in_buffer - total_size_requested;

	//Let's create a temporary store of the data we have
	char * temp_data = (char *)malloc((sizeof(char))*data_in_buffer);
	memcpy(temp_data , f->buffer[f->buffer_read_head] , data_in_buffer);

	//Now let's write the requested data in the buffer first
	int actual_write_data = min(total_size_requested , BUFFER_SIZE);
	memcpy(f->buffer,p,actual_write_data);

	//Update the dirty write

	//is there any space remaining?



	//Now let's copy the

	int place_available = BUFFER_SIZE - f->buffer_read_tail;

	if(place_available < total_size_requested){
		//We do not have place to write data, let's create some space
		if(total_size_requested < BUFFER_SIZE){

		}
	}else{

		//We have space to write data in the buffer
		memcpy(f->buffer[f->buffer_read_tail], p , total_size_requested);
		f->buffer_read_tail += total_size_requested;
	}

	return 0;
}
 */


#endif
