#include <stdlib.h>
#include "my_stdio.h"


MY_FILE *my_fopen(char *name, char *mode){
	MY_FILE *fp = (MY_FILE *) malloc(sizeof(MY_FILE));
	fp->buffer = malloc(sizeof(char)*BUFFER_SIZE);
	strcpy(fp->mode,mode);

	//open the requested file and read from it into the buffer
	fp->fp = fopen(name , mode);

	if(strcmp(fp->mode, "r") ==0 ){
		printf("Opened file in read mode\n");
		//try to read the whole buffer size from the file
		fp->buffer_read_tail = read(fp->fp, fp->buffer,1* BUFFER_SIZE);
		fp->buffer_read_head = 0;
		fp->size_dirty_bytes = 0;
	}else{
		printf("Opened file in write mode\n");
		fp->buffer_read_head = 0;
		fp->buffer_read_tail = 0;
		fp->size_dirty_bytes = 0;
	}

	return fp;
}

int my_fclose(MY_FILE *f){
	if(f->buffer_read_tail - f->buffer_read_head > 0){
		//write the full buffer to file
		write ( f->fp , f->buffer + f->buffer_read_head, sizeof(char)* ( f->buffer_read_tail - f->buffer_read_head ));
	}
	fclose(f->fp);
	return 0;
}

int my_fread(void *p, size_t size, size_t nbelem, MY_FILE *f){

	if(strcmp(f->mode, "r") !=0 ){
		return -1;
	}

	int actual_read = 0;
	int total_size_requested = size * nbelem;

	if(total_size_requested <= 0){
		//Invalid input
		return 0;
	}

	int data_in_buffer = f->buffer_read_tail - f->buffer_read_head;

	if(total_size_requested <= data_in_buffer )
	{
		//If we have the requested amount of data just copy it into the buffer
		memcpy(p , f->buffer+f->buffer_read_head , total_size_requested);
		f->buffer_read_head += total_size_requested;
		actual_read = total_size_requested;
	}else{
		//User requested a lot, we will provide as much as we can
		memcpy(p , f->buffer[f->buffer_read_head] , data_in_buffer);
		f->buffer_read_head += data_in_buffer;
		actual_read = data_in_buffer;
	}

	//Is our buffer aligned properly? Have we moved half into the buffer?
	if(f->buffer_read_head > (BUFFER_SIZE - 0)/2){
		int i=0,j =0;
		for(i =f->buffer_read_head;i<=f->buffer_read_tail ; i++,j++)
			f->buffer[j] = f->buffer[i];

		//Update the new values
		//Tail will be just the size of the data
		f->buffer_read_tail = f->buffer_read_tail - f->buffer_read_head;
		f->buffer_read_head = 0;

		//TODO : Update the buffer dirty write head too
	}

	//printf("in my_fread buffer = %s p = %s\n" , f->buffer, p);

	//Now we will try to fetch more
	f->buffer_read_tail +=  read(f->fp , f->buffer,1 * ( f->buffer_read_tail - f->buffer_read_head));

	return actual_read;
}

/*
 * Write to the read head
 */
int my_fwrite(void *p, size_t taille, size_t nbelem, MY_FILE *f){

	if(strcmp(f->mode, "w") !=0 ){
		return -1;
	}

	int requested_size = taille * nbelem;
	int remaining_space = f->buffer_read_tail - f->buffer_read_head;
	int actual_write = 0;

	if(requested_size <= remaining_space){
		//Just copy the data in the buffer
		actual_write = requested_size;
		memcpy(f->buffer+f->buffer_read_tail ,p , actual_write );
		f->buffer_read_tail += actual_write;
	}else{
		//We do not have enough space left,
		//Write as much as I can into remaining space, i.e fill up the buffer
		memcpy(f->buffer+f->buffer_read_tail ,p , remaining_space );
		requested_size -= remaining_space;
		actual_write += remaining_space;

		//write the filled buffer to the file
		write (f->fp , f->buffer , sizeof(char) * (f->buffer_read_tail - f->buffer_read_head));

		f->buffer_read_head = 0;
		f->buffer_read_tail = 0;

		if(requested_size <= BUFFER_SIZE){
			//just copy to our buffer and keep
			memcpy(f->buffer+f->buffer_read_tail ,p+actual_write , requested_size );
			f->buffer_read_tail += requested_size;
		} else{
			//write to file directly
			write (f->fp ,p+actual_write, sizeof(char)* requested_size );

		}

		actual_write += requested_size;
	}

	//printf("in my_write buffer = %s p = %s\n" , f->buffer, p);

	return actual_write;
}

int main (int argc, char *argv[])
{
	MY_FILE *f1;
	MY_FILE *f2;
	char c;
	int result;
	// for the sake of simplicity we don’t
	// print any error messages
	if (argc != 3)
		exit (-1);
	f1 = my_fopen(argv[1], "r");
	if (f1 == NULL)
		exit (-2);
	f2 = my_fopen(argv[2], "w");
	if (f2 == NULL)
		exit (-3);
	result = my_fread(&c, 1, 1, f1);
	while (result == 1)
	{
		result = my_fwrite(&c, 1, 1, f2);
		if (result == -1)
			exit(-4);
		result = my_fread(&c, 1, 1, f1);
	}
	if (result == -1)
		exit(-5);
	my_fclose(f1);
	my_fclose(f2);
	return 0;
}
