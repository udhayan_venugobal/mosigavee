# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 14:38:49 2015

@author: aveekmukherjee
"""

import numpy as np
import matplotlib.pyplot as plt

# import your own functions
from ispFunctions import myMean, myStandardDeviation , myHistogram


predicted = [996, 868, 855, 956, 867, 933, 866, 887, 936, 901, 818, 956]

predicted = np.array(predicted)
actual = np.array([0,876])

plt.figure()
line = plt.plot( predicted, '--', marker='o', linewidth=2 , color = 'red' , label = "Predicted by experts")
line2 = plt.plot( [0,12],[876,876] , linewidth=2 ,color = 'green' , label ="Actual price")
plt.grid()
plt.title('Stock price prediction');
plt.ylabel('Stock price')
plt.xlabel('Expert')
plt.legend()
plt.show()

mean = myMean(predicted)
stdev = myStandardDeviation(predicted)

print("The mean is : "+str(mean) + " and the standard deviation is : "+str(stdev))

print("The histogram is " + str(myHistogram(predicted,20)))
