# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 14:09:22 2015

Exercise on 1D discrete fourier transform

@author: aveekmukherjee
"""
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 500,500)
x1 = np.sin(2*np.pi*x/100)

def DFT1D(x):
    X= []
    k = 0
    i = complex(0,1)
    N = len(x)
    for k in range(0,N):
        #lets compute xk
        Xk = 0
        for n in range(0,N):
            Xk += x[n]*np.exp(-2*i*np.pi*k*n/N)
        X.append(Xk)
    return X

def myInvDFT1D(x):
    X= []
    k = 0
    i = complex(0,1)
    N = len(x)
    for k in range(0,N):
        #lets compute xk
        Xk = 0
        for n in range(0,N):
            Xk += x[n]*np.exp(2*i*np.pi*k*n/N)
        Xk = Xk/N
        X.append(Xk)
    return X


plt.figure()
plt.plot(x, x1 , color = 'red' , label = "x1")
plt.plot(x, DFT1D(x1) , color = 'blue' , label = "dft x1")
plt.legend()

plt.figure()
plt.plot(x, myInvDFT1D(DFT1D(x1)) , color = 'red' , label = "idft")
#plt.plot(x, np.fft.ifft(DFT1D(x1) ) , color = 'green' , label = "fft x1")
plt.grid()
plt.legend()  
     
def generateDelta(IdxSampleWhereImpuls , numberOfSamples):
    x=[]
    for i in range(0,numberOfSamples):
        if(i in IdxSampleWhereImpuls):
            x.append(1)
        else:
            x.append(0)
    return x
    
def generateGraph(X):
    x = np.linspace(0, 50,50)
    
    plt.figure()
    plt.grid()
    
    plt.plot(x,np.real(X),color = 'red' , label ="real")
    plt.plot(x,np.imag(X),color = 'blue' , label ="imag")
    plt.plot(x,np.angle(X),color = 'green' , label ="angle")
    plt.plot(x,np.abs(X),color = 'purple' , label ="abs")
    #plt.ylim(0,1.2)
    plt.legend()
    
x1=generateDelta([4],50)
x = np.linspace(0, 50,50)

plt.figure()
plt.plot(x, x1 , color = 'red' , label = "x1")
#plt.plot(x, DFT1D(x1) , color = 'blue' , label = "ft x1")
generateGraph(DFT1D(x1))
plt.legend()

plt.figure()
plt.plot(myInvDFT1D(DFT1D(x1)))
