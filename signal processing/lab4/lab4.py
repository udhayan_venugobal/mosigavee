# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 14:15:17 2015

@author: aveekmukherjee
"""

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from scipy.fftpack import fft2,fftshift,ifft2
import re 

image_file = "wheele.pgm"
#image_file = "damierHV.pgm"
# Function to read a pgm image from a file
def read_pgm(filename, byteorder='>'):
    """Return image data from a raw PGM file as numpy array.
 
    Format specification: http://netpbm.sourceforge.net/doc/pgm.html
 
    """
    with open(filename, 'rb') as f:
        buffer = f.read()
    try:
        header, width, height, maxval = re.search(
            b"(^P5\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", buffer).groups()
    except AttributeError:
        raise ValueError("Not a raw PGM file: '%s'" % filename)
    return np.frombuffer(buffer,
                            dtype='u1' if int(maxval) < 256 else byteorder+'u2',
                            count=int(width)*int(height),
                            offset=len(header)
                            ).reshape((int(height), int(width)))

image = read_pgm(image_file)
        
def power_spectrum(image):
    return np.log(np.abs(image)**2+np.finfo(float).eps)
    

sum_pixels = np.sum(image)

print(sum_pixels)
plt.figure()                      
plt.imshow(image, plt.cm.gray)

fftimg = fft2(image)
#plt.figure()
#plt.imshow(np.log(fftimg.real))

#print(fft2(image))
#for the first term in 1D, the exp part become e(0), hence its only real part, extend this logic to 2D

plt.figure()
plt.ylabel("fy")
plt.xlabel("fx")
plt.imshow(power_spectrum(fftimg))

l,w = image.shape
xmin = -l/2 + 1
xmax = l/2 
ymin = 0
ymax = w

plt.figure()
plt.ylabel("fy")
plt.xlabel("fx")
plt.imshow(power_spectrum(fftshift(fftimg)) , extent = [xmin , xmax , ymin , ymax])



plt.figure()
ifftimg = np.rint(ifft2(fftimg))
plt.imshow(np.real(ifftimg), plt.cm.gray)