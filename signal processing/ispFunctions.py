# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 15:42:24 2015

@author: aveekmukherjee
"""

def myMin(x):
    m = x[0]
    for k in range(len(x)):
        if (x[k] < m):
            m = x[k]        
    return m
    
def myMax(x):
    m = x[0]
    for k in range(0, len(x)):
        if (x[k] > m):
            m = x[k]        
    return m
    
def myMean(x):
    s = 0
    n = len(x)
    for k in range(0,n):
        s=s+x[k]
    
    return s/n
        
def myStandardDeviation(x):
    mu = myMean(x)
    n = len(x)
    s=0
    for k in range(0,n):
        s= s+ (x[k] - mu)**2
    
    return (s/(n-1))**0.5
    
def myHistogram(x,intervalLength):
    minVal = myMin(x)
    maxVal = myMax(x)
    n = len(x)
    size = int((maxVal - minVal)/intervalLength)
    print("size = "+str(size))
    histo = [0]*(size+1)
    
    for k in range(0,n):
        j = int((x[k] - minVal) / intervalLength)
        #print ("j =" +str(j));
        histo[j] = histo[j]+1
    
    return histo
    